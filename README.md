# Blog resource on how to replay a pipeline with Jenkins

This repository is a fork of [the official Jenkins workflow demo](https://github.com/jenkinsci/workflow-aggregator-plugin).

It serves as a base to [my blog post](http://blog.huitelec.fr/2017/01/24/replay-a-jenkins-build-pipeline-locally/).

Know more about the [demo itself](https://github.com/fhuitelec/blog-resource-jenkins-replay-example/blob/master/demo/README.md).
