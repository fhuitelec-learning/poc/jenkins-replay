cli-replay:
	docker exec -i jenkins_jenkins_1 bash -c 'cd /application/bin && \
	./jenkins-replay \
	replay-pipeline cd/master \
	-s Jenkinsfile < /application/demo/repo/Jenkinsfile \
	-s demo.Servers < /application/demo/lib/src/demo/Servers.groovy \
	'

init:
	docker exec -i jenkins_jenkins_1 bash -c ' \
		cd /application/vendor/jenkins && \
		wget http://localhost:8080/jnlpJars/jenkins-cli.jar \
	'